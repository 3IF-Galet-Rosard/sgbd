## Préambule

```sql
DROP SYNONYM t;
CREATE SYNONYM t FOR agalet.T;
```

## Questions

### Q2.1

Requêtes à exécuter :
```sql
-- t3
INSERT INTO t VALUES (0,99);
-- t4
COMMIT;
```

Résultat :
| ligne | transaction | isolation | mode | observations |
|---|---|---|---|---|
| 2 |  | ? | Lecture/Écriture | On voit l'état actuel de la table commun aux deux utilisateurs. |
| 4 |  | ? | Lecture/Écriture | Les modification ne sont pas encore visibles. |
| 6 |  | ? | Lecture/Écriture | Les modifications sont visibles. |

Conclusion :  
On remarque que l'on n'est pas en `READ UNCOMMITED`.

### Q2.1

Remise dans l'état initial :
```sql
DELETE FROM t 
WHERE t.B = 99;
```

Requêtes à exécuter :
```sql
-- t3
INSERT INTO t VALUES (3,7);
-- t5
SELECT * FROM t;
-- t6 
DELETE FROM t WHERE a=2;
-- t8 
DELETE FROM t WHERE a=1;
-- t10 
SELECT * FROM t;
-- t11 
COMMIT;
```

Observations :
| ligne | transaction | isolation | mode | observations |
|---|---|---|---|---|
| 2 | 1 | READ COMMITED | Lecture/Écriture | Les tuples ont changé. |
| 4 | 1 | READ COMMITED | Lecture/Écriture | Les modifications de la ligne 3 ne sont pas visibles. |
| 6 | 2 | READ COMMITED | Lecture/Écriture | La requête est en attente de commit. |
| 7 | 1 | READ COMMITED | Lecture/Écriture | La requête précédente s'exécute après le commit. 0 lignes sont supprimées. |
| 9 | 3 | READ COMMITED | Lecture/Écriture | Toujours aucune différence par rapport à l'exécution de la ligne 2. |
| 10 | 2 | READ COMMITED | Lecture/Écriture | Les modifications de la session 2 sont visibles par la session 2. |
| 12 | 3 | READ COMMITED | Lecture/Écriture | Les modifications de la session 2 sont visibles par la session 1. |

### Q2.3 

Requêtes à exécuter :
```sql
-- t2
UPDATE t SET a=1 WHERE a=0 and b=1;
-- t4
SELECT * FROM t;
-- t6
SELECT * FROM t;
-- t7
COMMIT;
-- t8
SELECT * FROM t;
```
Observations :
- Ligne 5: Le verrou posé sur le tuple lors de la ligne 2 bloque l'instruction `UPDATE`. 
- Ligne 7 : Le commit ajoute les modifications, et donc l'instruction `UPDATE` s'exécute mais le tuple a été modifié, donc la requuête modifie 0 lignes.

Conclusion :  
Les verrous des updates ne sont présents que pour les tuples et non la tables en entier.

### Q2.4

Requêtes à exécuter :
```sql
-- t2
UPDATE t SET b=10 WHERE B=3;
-- t3 
SELECT * FOR t;
-- t5
UPDATE T SET b=6 WHERE a=0 and b=0;
-- t9
SELECT * FROM t;
-- t11
ROLLBACK;
-- t12
SELECT * FROM t;
```
Observations :
- Ligne 5 : Le `SFW FOR UPDATE` a verrouillé des tuples et donc cette instruction est en attente.

Conclusion :  
Les verrous disparaissent après un ROLLBACK.

### Q2.5

Note :
La session1 est en mode `READ ONLY`.
Requêtes à exécuter :
```sql
-- t3
SELECT * FROM t;
-- t4 
UPDATE t SET a=100 WHERE a=2;
-- t5 
SELECT * FROM t;
-- t7
COMMIT;
-- t9
UPDATE t SET a=a+10;
-- t10
COMMIT;
```

Observations :
- ligne 8 : Malgré le commit, la session1 ne voit pas les modifications car étant en mode `READ ONLY`, son niveau d'isolation est `SERIALIZABLE`.
- ligne 14 : La transactionp précédente étant finie, les modifications de la session2 sont visibles par la session.
- ligne 15 : La transaction précédente étant terminée, le mode redevient `READ/WRITE` et l'instruction `UPDATE` est possible.
