## Préambule

```sql
CREATE TABLE T (a INTEGER, b INTEGER);
GRANT SELECT, INSERT, DELETE ON t TO AROSARD;
INSERT INTO t VALUES (0,0);
INSERT INTO t VALUES (2,3);
INSERT INTO t VALUES (0,1);
COMMIT;
```

## Questions

### Q2.1

```sql
-- t1
SELECT * FROM t WHERE a=0;
-- résultat : 2 tuples
```

```sql
-- t3
SELECT * FROM t WHERE a=0;
-- résultat : 2 tuples
```

```sql
-- t5
SELECT * FROM t WHERE a=0;
-- résultat : 3 tuples
```

Le même requête a eu des résultas différents

### Q2.2

```sql
--t1
UPDATE t SET A=a+1;
--t2
SELECT * FROM t;
```

Les tuples sont modifiés

```sql
--t4
SELECT * FROM t;
```

Les tuples n'ont pas changés.

```sql
--t7
COMMIT
```

```sql
--t9
SELECT * FROM t;
```

Les tuples n'ont pas changés.

```sql
--t12
SELECT * FROM t;
```

Les tuples ont changés

### Q2.3

```sql
--t1
UPDATE t SET a=1 WHERE a=0 AND b=0;
```

1 ligne mise à jour

```sql
--t3
SELECT * FROM t;
```

Le tuple est modifié

```sql
--t5
UPDATE t SET b=3 WHERE a=0 AND b=1;
```

En attente puis 0 lignes mises à jour

```sql
--t9
SELECT * FROM t;
```

```sql
--t10
COMMIT;
```

### Q2.4

```sql
--t1
SELECT * FROM t WHERE A=0 FOR UPDATE;
```

RAS

```sql
--t4
SELECT * FROM t;
```

RAS

```sql
--t6
UPDATE t SET b=16 WHERE a=0 AND b=0;
```
RAS


```sql
--t7
SELECT * FROM t;
```

RAS, les changements sont passés

```sql
--t8
COMMIT;
```

```sql
--t10
SELECT * FROM t;
```

Pas de changement visible

### Q2.5

```sql
--t1
SET TRANSACTION READ ONLY;
--t2
SELECT * FROM t;
```

```sql
--t6
SELECT * FROM t;
```

Aucun changement

```sql
--t8
SELECT * FROM t;
```

Aucun changement

```sql
--t11
SELECT * FROM t;
--t12
UPDATE t SET a=b+1;
```

Modification impossible car la transaction est read-only

```sql
--t13
COMMIT;
--t14
SELECT * FROM t;
--t15
UPDATE t SET a=b+1;
--t16
COMMIT;
```

Modification possible car COMMIT = fin de transaction