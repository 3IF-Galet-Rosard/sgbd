-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308

ALTER TABLE Cinema
ADD CONSTRAINT ckAdresseNN
CHECK (adresse IS NOT NULL);

-- test d'insertion incorrecte
INSERT INTO Cinema VALUES
    ('Pate', 1, NULL); 

-- test d'insertion correcte
INSERT INTO Cinema VALUES
    ('Pate', 1, 'rue de la gare'); 
