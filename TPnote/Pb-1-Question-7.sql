-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308

DROP TABLE JoueDans;
DROP TABLE EstProjete;
DROP TABLE Film;
DROP TABLE Artiste;
DROP TABLE Salle;
DROP TABLE Cinema;

-- les tables sont supprimées selon les clés étrangeres qui
-- reférencent leur colonnes depuis les autres tables
-- on supprime d'abord les tables dont aucune des colonnes n'est la cible
-- d'aucune cle etrangere : JoueDans, EstProjete

-- ensuite celles dont les cles etrangeres des autres tables ont deja été supprimées
-- Film puis Artiste et Salle puis Cinema