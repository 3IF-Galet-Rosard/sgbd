-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308

ALTER TABLE Artiste
ADD CONSTRAINT ckNomMaj
CHECK (nom=UPPER(nom));

-- test d'insertion incorrecte
INSERT INTO Artiste VALUES
    (1, 'Dujardin', 'Jean', TO_DATE('01/01/1998', 'DD/MM/YYYY'));
    
-- test d'insertion correcte
INSERT INTO Artiste VALUES
    (1, 'DUJARDIN', 'Jean', TO_DATE('01/01/1998', 'DD/MM/YYYY'));

