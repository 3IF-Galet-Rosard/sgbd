-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308

SELECT max(PaysTraversés)
FROM (
    SELECT count(DISTINCT name) as PaysTraversés
    FROM (
        Country
    INNER JOIN
        Geo_River
    ON Country.code = Geo_River.country)
    GROUP BY river);