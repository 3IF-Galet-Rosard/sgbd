-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308

ALTER TABLE Salle
ADD CONSTRAINT ckCapa30
CHECK (capacite>=30);

-- en supposant que les test d'insertions de cinema ont ete effectues
INSERT INTO Cinema VALUES
    ('Gomon', 2, 'avenue de la republique'); 

-- test d'insertion incorrecte
INSERT INTO Salle VALUES
    (8, 29, 'Gomon');

-- test d'insertion correcte    
INSERT INTO Salle VALUES
    (8, 30, 'Gomon');