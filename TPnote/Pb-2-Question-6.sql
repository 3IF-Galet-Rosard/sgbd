-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308

SELECT name 
FROM Country
WHERE code NOT IN (
    SELECT DISTINCT Country
    FROM Geo_River
);