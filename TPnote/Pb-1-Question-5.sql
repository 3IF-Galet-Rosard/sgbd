-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308

CREATE OR REPLACE TRIGGER trgDateRoleActeur
BEFORE INSERT OR UPDATE
ON joueDans FOR EACH ROW
DECLARE
    DN DATE;
    DS DATE;
    num INT;
    invalide EXCEPTION;
    noArtiste EXCEPTION;
    noFilm EXCEPTION;
BEGIN
    -- test d'existence de l'artiste
    SELECT Count(*) INTO num
    FROM Artiste
    WHERE idArtiste = :NEW.idArtiste;
    IF (num!=1) THEN
        RAISE noArtiste;
    END IF;

    SELECT DateNaissance INTO DN
    FROM Artiste
    WHERE idArtiste = :NEW.idArtiste;

    -- test d'existence du film
    SELECT Count(*) INTO num
    FROM Film
    WHERE Titre = :NEW.TitreFilm;

    IF (num!=1) THEN
        RAISE noFilm;
    END IF;

    SELECT DateSortie INTO DS
    FROM Film
    WHERE Titre = :NEW.TitreFilm;

    IF (DS < DN) THEN
        RAISE invalide;
    END IF;
    
    EXCEPTION
        WHEN invalide THEN
            dbms_output.put_line('Acteur plus jeune que le film');
        WHEN noArtiste THEN
            dbms_output.put_line('Cet acteur nexiste pas');
        WHEN noFilm THEN
            dbms_output.put_line('Ce film nexiste pas');
END;


INSERT INTO Artiste VALUES
    (10, 'DUJARDIN', 'Jean', TO_DATE('01/01/1998', 'DD/MM/YYYY'));

INSERT INTO Film VALUES
    ('OSS 117', TO_DATE('01/01/1960', 'DD/MM/YYYY'), 1);

INSERT INTO JoueDans VALUES
    (10, 'Noel Flantier', 'OSS 117');

