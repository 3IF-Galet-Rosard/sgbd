-- Contrôle de TP
--            NumEtudiant      Nom      Prénom 
-- Membre 1 : 4023114          Rosard   Alexandre
-- Membre 2 : 4027962          Galet    Arthur

CREATE TABLE Cinema (
    Nom VARCHAR2(100),
    Arrondissement INT,
    Adresse VARCHAR2(100),
    CONSTRAINT pkCinema
        PRIMARY KEY (Nom)
);

CREATE TABLE Artiste (
    IdArtiste INT,
    Nom VARCHAR2(100),
    Prenom VARCHAR2(100),
    DateNaissance DATE,
    CONSTRAINT pkArtiste
        PRIMARY KEY (IdArtiste)
);

CREATE TABLE Salle (
    Numero INT,
    Capacite INT,
    NomCinema VARCHAR2(100),
    CONSTRAINT pkSalle
        PRIMARY KEY (Numero,NomCinema),
    CONSTRAINT fkSalleCinema
        FOREIGN KEY (NomCinema)
        REFERENCES Cinema(Nom)
);

CREATE TABLE Film (
    Titre VARCHAR2(100),
    DateSortie DATE,
    IdRealisateur INT,
    CONSTRAINT pkFilm
        PRIMARY KEY (Titre),
    CONSTRAINT fkFilmArtiste
        FOREIGN KEY (IdRealisateur)
        REFERENCES Artiste(IdArtiste)
);

CREATE TABLE EstProjete (
    idProjection INT,
    TitreFilm VARCHAR2(100),
    NumeroSalle INT,
    NomCinemaSalle VARCHAR2(100),
    DateProjection DATE,
    HeureDebut VARCHAR2(5),
    HeureFin VARCHAR2(5),
    CONSTRAINT pkEstProjete
        PRIMARY KEY (idProjection),
    CONSTRAINT fkEstProjeteFilm
        FOREIGN KEY (TitreFilm)
        REFERENCES Film(Titre),
    CONSTRAINT fkEstProjeteSalle
        FOREIGN KEY (NumeroSalle,NomCinemaSalle)
        REFERENCES Salle(Numero,NomCinema)
);

CREATE TABLE JoueDans (
    idArtiste INT,
    NomRole VARCHAR2(100),
    TitreFilm VARCHAR2(100),
    CONSTRAINT pkJoueDans
        PRIMARY KEY (idArtiste,TitreFilm),
    CONSTRAINT fkJoueDansArtiste
        FOREIGN KEY (idArtiste)
        REFERENCES Artiste(idArtiste),
    CONSTRAINT fkJoueDansFilm
        FOREIGN KEY (TitreFilm)
        REFERENCES Film(Titre)
);