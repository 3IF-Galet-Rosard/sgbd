-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308
SELECT DISTINCT name
FROM Country
WHERE code IN (
    SELECT country
    FROM Geo_River
    WHERE river = 'Donau');    

