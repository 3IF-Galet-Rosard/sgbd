-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308
SELECT name, length
FROM River
WHERE length IN (
    SELECT max(length)
    FROM River);
