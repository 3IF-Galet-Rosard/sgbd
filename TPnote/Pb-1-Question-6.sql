-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308

DELETE FROM Artiste WHERE idArtiste=1;
-- 1 etant l'IdArtsite (clé) de l'artiste a supprimer

-- l'artiste ne peut etre supprimé que s'il n'est pas référencé
-- depuis une autre table avec une clé étrangère et donc :
-- il n'est le realisateur d'aucun film
-- il ne joue dans aucun film