-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308
SELECT name, rivieresQuiTraversent
FROM (
    Country
LEFT OUTER JOIN
    (SELECT country as coucou,count(DISTINCT river) as rivieresQuiTraversent
    FROM Geo_River
    GROUP BY country)
ON Country.code = coucou)
ORDER BY rivieresQuiTraversent DESC, name ASC;