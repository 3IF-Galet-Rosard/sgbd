-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308
SELECT continent,river.name
FROM(
    SELECT DISTINCT continent,river.length rl
    FROM((
            encompasses
        LEFT OUTER JOIN
            Geo_River
        ON Encompasses.country = Geo_River.country)
    INNER JOIN 
        River
    ON geo_river.river = river.name)
INNER JOIN 
    (SELECT *
    FROM River)
ON River.length = rl);

