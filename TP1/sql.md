### Q1 Ajouter une contrainte de clef étrangère

```sql
ALTER TABLE COUNTRY 
ADD CONSTRAINT fkCountryCity
FOREIGN KEY (CAPITAL,CODE,PROVINCE) 
REFERENCES CITY(NAME,COUNTRY,PROVINCE);
```

### Q2 Ajouter une contrainte d'unicité pour le nom du pays
Le schéma l'intègre déjà.

```sql
ALTER TABLE COUNTRY 
ADD CONSTRAINT uniqueCapital
UNIQUE (NAME);
```

### Q3 Ajouter une contraite NON NULL pour le nom du pays
Le schéma l'intègre déjà.

```sql
ALTER TABLE COUNTRY 
ADD CONSTRAINT notNullCapital
CHECK (NAME) IS NOT NULL;
```

### Q4 Ajouter une contrainte de clef étrangère pour la capitale de province

```sql
ALTER TABLE PROVINCE 
ADD CONSTRAINT fkProvinceCity
FOREIGN KEY (CAPITAL,COUNTRY,CAPPROV) 
REFERENCES CITY(NAME,COUNTRY,PROVINCE);
```
La requête est impossible:
Une ville peut être capitale de deux provinces.



### Q5 Ajouter une contrainte de clef étrangère pour le pays de province

```sql
ALTER TABLE PROVINCE
ADD CONSTRAINT fkProvinceCountry 
FOREIGN KEY (COUNTRY) 
REFERENCES COUNTRY(CODE);
```

### Q6 Ajouter une contrainte de clef étrangère pour le pays d'une ville

```sql
ALTER TABLE CITY
ADD CONSTRAINT fkCityCountry 
FOREIGN KEY (COUNTRY) 
REFERENCES COUNTRY(CODE);
```

### Q7 Ajouter une contrainte de clef étrangère pour la province d'une ville

```sql
ALTER TABLE CITY
ADD CONSTRAINT fkCityProvince 
FOREIGN KEY (PROVINCE,COUNTRY) 
REFERENCES PROVINCE(NAME,COUNTRY);
```

### Q8 Ajouter une contrainte de valeur d'attribut
Le schéma l'intègre déjà.

### Q9 Ajouter une contrainte d'unicité entre les coordonnées d'une ville

```sql
ALTER TABLE CITY
ADD CONSTRAINT uniqueCityLatituteLongitude
UNIQUE (LATITUDE,LONGITUDE);
```

Des tuples ne respectent pas cette contrainte, il n'est donc pas possible de l'implémenter telle quelle.
Pour obtenir lesquels :

```sql
SELECT C1.NAME,C2.NAME,C2.LATITUDE,C2.LONGITUDE
FROM CITY C1,CITY C2
WHERE C1.LATITUDE = C2.LATITUDE AND C1.LONGITUDE = C2.LONGITUDE 
AND (C1.NAME>C2.NAME OR C1.COUNTRY>C2.COUNTRY OR C1.PROVINCE>C2.PROVINCE);
```

# Ex2 :

## Requêtes

### Q1

Solution 1 : Utiliser des NULL (mauvais)
```sql
INSERT INTO COUNTRY(NAME,CODE,CAPITAL,PROVINCE,AREA,POPULATION) VALUES ('Bascrit','BC',NULL,'Bascrit',1250,1250000);
INSERT INTO PROVINCE(NAME,COUNTRY,POPULATION,AREA,CAPITAL,CAPPROV) VALUES ('Bascrit','BC',1250000,1250,'Demoi','Bascrit');
ROLLBACK;
```
Solution 2 : Désactiver des contraintes temporairement (mauvais)
Solution 3 : Défération     
```sql
ALTER SESSION SET CONSTRAINTS = DEFERRED;
INSERT INTO COUNTRY(NAME,CODE,CAPITAL,PROVINCE,AREA,POPULATION) VALUES ('Bascrit','BC','Demoi','Bascrit',1250,1250000);
INSERT INTO PROVINCE(NAME,COUNTRY,POPULATION,AREA,CAPITAL,CAPPROV) VALUES ('Bascrit','BC',1250000,1250,'Demoi','Bascrit');
COMMIT;
```

### Q2

```sql
SELECT *
FROM COUNTRY
ORDER BY NAME;
```

### Q3

```sql
SELECT b.NAME,b.ABBREVIATION,a.mbnb 
FROM (SELECT ORGANIZATION, count(*) as mbnb
    FROM ISMEMBER
    WHERE TYPE='member'
    GROUP BY ORGANIZATION
    ORDER BY mbnb DESC) a LEFT JOIN ORGANIZATION b ON a.ORGANIZATION = b.ABBREVIATION
WHERE ROWNUM <= 10;
```

### Q4

```sql
SELECT CODE, (popU/popT*100) prct 
FROM 
    (SELECT CODE, COUNTRY.POPULATION popT, SUM(CITY.POPULATION) popU
    FROM CITY INNER JOIN COUNTRY
    ON CITY.COUNTRY = COUNTRY.CODE
    WHERE CITY.POPULATION IS NOT NULL AND COUNTRY.POPULATION IS NOT NULL
    GROUP BY CODE, COUNTRY.POPULATION) 
ORDER BY prct ASC
```

### Q5

```sql
SELECT CONTINENT,SUM(POPULATION) as pop 
FROM ENCOMPASSES 
INNER JOIN (SELECT CODE,POPULATION FROM COUNTRY) 
ON ENCOMPASSES.COUNTRY= CODE
GROUP BY CONTINENT
ORDER BY pop DESC;
```

### Q6

Bonne solution utilisant un `OR` sur l'attribut d'un `JOIN` :
```sql
SELECT code, totbord
FROM (SELECT code, SUM(length) as totbord
    FROM (country JOIN borders ON (country1 = code OR country2 = code))
    GROUP BY code
    ORDER BY totbord DESC)
WHERE ROWNUM = 1;
```
Mauvaise solution qui utlisie un `UNION` et un renommage (aussi appelée OU-phobie) :
```sql
SELECT * 
FROM (SELECT C1, SUM(LENGTH) longueur
    FROM((SELECT COUNTRY1 C1, COUNTRY2 C2, LENGTH
        FROM BORDERS)
    UNION
        (SELECT COUNTRY2 C1, COUNTRY1 C2, LENGTH
        FROM BORDERS))
    GROUP BY (C1)
    ORDER BY longueur DESC)
WHERE ROWNUM <= 1;
```

### Q7 

```sql
SELECT name, count(country) as occurences
FROM (SELECT DISTINCT name, country 
    FROM city)
GROUP BY name
HAVING count(country)>1
ORDER BY occurences DESC, name;SELECT name, count(country) as occurences
FROM (SELECT DISTINCT name, country 
    FROM city)
GROUP BY name
HAVING count(country)>1
ORDER BY occurences DESC, name;
```

### Q8

```sql
SELECT name, code
FROM (country 
    JOIN (SELECT country cod
        FROM politics 
        WHERE independence IS NULL)
    ON code = cod);
```
Notons l'utilisation du `WHERE x IS NULL` avant le `JOIN` pour réduire le coût de l'exécution.

### Q10

```sql
SELECT country.name, organization.name
FROM (  (country  
        LEFT OUTER JOIN ismember 
        ON ismember.country = country.code)
    LEFT OUTER JOIN organization 
    ON ismember.organization = organization.abbreviation)    
ORDER BY country.name, organization.name;
```

### Q15

```sql
SELECT * 
FROM (SELECT language.name, SUM(language.percentage*country.population)/100 as locuteurs
    FROM (language 
        LEFT JOIN country 
        ON country.code = language.country)
    GROUP BY language.name
    ORDER BY SUM(language.percentage*country.population)/100 DESC)
WHERE ROWNUM < 6; 
```

### Q17 Création et utilisation des différents index

#### 1
Table : Population
Clée primaire : country
Index : normal

#### 2

```sql
SELECT *
FROM population
WHERE country = 'F';
```
(option plan d'explication à côté de l'icône pour exécuter le script)
L'index `POPKEY` est utilisé, avec l'option `UNIQUE SCAN` (= lecture unique dans la table).

#### 3

```sql
SELECT *
FROM population
WHERE country <'F';
```
Un index est utilisé, c'est l'attribut `country`.
L'index `POPKEY` est utilisé, avec l'option `RANGE SCAN` (= lecture partielle dans la table).

#### 4 

```sql
SELECT *
FROM population
WHERE population_growth = 0;
```
Aucun index n'est utilisé.

#### 5 Test de l'index B-tree sur une égalité

```sql
CREATE INDEX POPGROWTHKEY 
ON POPULATION(population_growth);
```

#### 6 Création d'un index bitmap
Suppression de l'index précédemment créé (2 index ne peuvent exister pour un même attribut/ensemble d'attributs).
```sql
DROP INDEX POPGROWTHKEY;
```

Création de l'index bitmap.
```sql
CREATE BITMAP INDEX POPGROWTHKEYBITMAP 
ON POPULATION(population_growth);
```

#### 7 Index bitmap et inégalité

Aucun index n'est utilisé, car l'index bitmap ne fonctionne pas pour l'égalité.

### Q18 Fonction PL/SQL

La fonction a créer et compiler est la suivante :
```sql
CREATE OR REPLACE FUNCTION NB_PAYS_FRONTALIERS 
(
  COUNTRY_CODE IN VARCHAR2 
) 
RETURN NUMBER
IS 
    total NUMBER :=0;
BEGIN
  --obtenir le nombre total de pays frontaliers
    SELECT count(*) 
    INTO total
    FROM (country 
        JOIN borders
        ON (country1 = code OR country2 = code))
    WHERE country.code = COUNTRY_CODE;
    
    --retourner le nombre total de pays frontaliers
    RETURN total;
END NB_PAYS_FRONTALIERS;
```
Pour tester cette fonction, on peut utiliser la procédure suivante : (pas encore finalisée ni testée)
```sql
create or replace PROCEDURE TEST_NB_PAYS_FRONTALIERS 
IS
    diff NUMBER;
BEGIN
    WITH
        PAYS_REQUETE AS
            (SELECT name, count(length)
            FROM
                (SELECT name, length 
                FROM country 
                LEFT OUTER JOIN borders
                ON (country1 = code OR country2 = code))
            GROUP BY name),
            
        PAYS_FONCTION AS    
            (SELECT name, NB_PAYS_FRONTALIERS(code)
            FROM country)
    
    SELECT count(*)
    INTO diff
    FROM (
        ((SELECT * FROM PAYS_FONCTION)
        MINUS (SELECT * FROM PAYS_REQUETE))
    UNION (
        (SELECT * FROM PAYS_REQUETE)
        MINUS (SELECT * FROM PAYS_FONCTION)))
    ;
    
    dbms_output.put_line(diff || ' differences');
END TEST_NB_PAYS_FRONTALIERS;
```

### Q19 

Pour ajouter a colonne :
```sql
ALTER TABLE country
ADD frontiereLongue varchar2(4 byte);
```

## Droits d'accès

### Q27

Pour ne pas faire la même commande pour chaque table, on va générer les commandes  grâce au script suivant.
```sql
SELECT 'GRANT SELECT, INSERT, UPDATE, DELETE ON ' || table_name || ' TO AROSARD'
FROM user_tables;
COMMIT;
```

Pour remplir la table, il faut exécuter la procédure PL/SQL suivante :
```sql
```

## Contraintes d'intégrité par les déclencheurs

## Traitement des données

## Modification des données