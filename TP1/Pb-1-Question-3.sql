-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308
ALTER TABLE Site
ADD CONSTRAINT SiteTypeValueSet
    CHECK (Type IN ('carriere','mine','grattage','sondage','afleurement'));