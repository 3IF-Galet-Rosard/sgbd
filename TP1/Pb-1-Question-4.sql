-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308
ALTER TABLE Mineraux
    ADD CONSTRAINT MinerauxNomMajuscule
        CHECK (Nom = UPPER(Nom));
