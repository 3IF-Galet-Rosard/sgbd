-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308

ALTER TABLE Echantillon 
ADD CONSTRAINT EchantillonRangementNotNull
    CHECK (Rangement IS NOT NULL);
