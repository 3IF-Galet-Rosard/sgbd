-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308
DROP TABLE Contient;
DROP TABLE Presence;
DROP TABLE Echantillon;
DROP TABLE Site;
DROP TABLE Commune;
DROP TABLE Mineraux;

CREATE TABLE Mineraux (
    Num INT,
    Nom VARCHAR2(100),
    FormuleChimique VARCHAR2(100),
    CONSTRAINT pkMineraux
        PRIMARY KEY (Num)
);

CREATE TABLE Commune (
    NumINSEE INT,
    Nom VARCHAR2(100),
    Dpt VARCHAR2(100),
    CONSTRAINT pkCommune
        PRIMARY KEY (NumINSEE)
);

CREATE TABLE Site (
    Num INT,
    Nom VARCHAR2(100),
    Type VARCHAR2(100),
    Actif VARCHAR2(100),
    Production VARCHAR2(100),
    Localisation INT,
    CONSTRAINT pkSite
        PRIMARY KEY (Num),
    CONSTRAINT fkSiteCommune
        FOREIGN KEY (Localisation)
        REFERENCES Commune(NumINSEE)
);

CREATE TABLE Echantillon (
    Num INT,
    Nom VARCHAR2(100),
    PrecisionNom VARCHAR2 (250),
    Rangement VARCHAR2(100),
    Provenance INT,
    DateDecouverte DATE,
    PrecisionLocalisation VARCHAR2(250),
    CONSTRAINT pkEchantillon
        PRIMARY KEY (Num),
    CONSTRAINT fkEchantillonSite
        FOREIGN KEY (Provenance)
        REFERENCES Site(Num)
);

CREATE TABLE Contient (
    NumEchantillon INT,
    NumMineraux INT,
    CONSTRAINT pkContient
        PRIMARY KEY (NumEchantillon,NumMineraux),
    CONSTRAINT fkContientEchantillon
        FOREIGN KEY (NumEchantillon)
        REFERENCES Echantillon(Num),
    CONSTRAINT fkContientMineraux
        FOREIGN KEY (NumMineraux)
        REFERENCES Mineraux(Num)
);

CREATE TABLE Presence (
    NumSite INT,
    NumMineraux INT,
    CONSTRAINT pkPresence
        PRIMARY KEY (NumSite,NumMineraux),
    CONSTRAINT fkPresenceSite
        FOREIGN KEY (NumSite)
        REFERENCES Site(Num),
    CONSTRAINT fkPresenceMineraux
        FOREIGN KEY (NumMineraux)
        REFERENCES Mineraux(Num)
);