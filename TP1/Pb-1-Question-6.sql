-- Alexandre Rosard | 4023114 - Arthur Galet | 4027962 - B3308
CREATE OR REPLACE FUNCTION nouveauMineralDansEchantillon(
    NumeroSite INT,
    NumeroEchantillon INT
) RETURN BOOLEAN
AS
DECLARE
    NombreNouveau INT;
BEGIN
    SELECT COUNT(*)
    INTO NombreNouveau
    FROM ((
        SELECT * 
        FROM Contient 
        WHERE NumEchantillon = NumeroEchantillon)
    LEFT OUTER JOIN (
        SELECT * 
        FROM Presence
        WHERE NumSite = NumeroSite))
    WHERE Presence.NumSite IS NULL;

    IF NombreNouveau = 0 THEN
        RETURN FALSE;
    ELSE 
        RETURN TRUE;
END;